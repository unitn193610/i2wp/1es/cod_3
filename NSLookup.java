package cod_3;

import java.net.InetAddress;
import java.net.UnknownHostException;

/*
    In quest'esercizio dobbiamo porre come argomento UN unico parametro,
    per farlo andare nelle impostazioni di Run dell'IDE.
    Il parametro dovrà essere il nome dell'host in modo da reperire il suo indirizzo IP.

    Nel mio caso pongo: mbp-nj

    Nel caso in cui non conoscessimo il nome del nostro Host, potremmo avvalerci dell'esercizio
    proposto nella directory cod_2.
*/

public class NSLookup
{
    public static void main(String[] args)
    {
        if (args.length != 1) {
            System.out.println("Usage: NSLookup <hostname>");
            System.exit(1);
            // È logico che dobbiamo interrompere l'esecuzione del codice
            // nel caso in cui vengano posti più di 1 parametro.
        }

        try {
            // Essendo l'oggetto address di tipo InetAddress,
            // possiamo recuperare le informazioni dell'host anche tramite il nome
            // col metodo getByName();
            InetAddress address = InetAddress.getByName(args[0]);

            // In assenza di errori dovremmo visualizzare il nome dell'host da noi inserito
            System.out.println(address.getHostName());

            // Pertanto, anche l'indirizzo IP tramite il metodo getHostAddress()
            System.out.println(address.getHostAddress());

        } catch (UnknownHostException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace(System.err);
        }
    }
}
